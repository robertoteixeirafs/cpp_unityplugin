#include "TestDLLSort.h"
#include <algorithm>

extern "C"{
	void TestSort(int a[], int length){
		std::sort(a, a + length);		
	}
	int TestMultiply(int a){
		return a * 10;
	}
}