﻿using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class CppPlugin : MonoBehaviour 
{

    [DllImport("UnityPlugin", EntryPoint = "TestMultiply")]
    public static extern int TestMultiply(int a);

    [DllImport("UnityPlugin", EntryPoint = "TestSort")]
    public static extern int TestSort(int a);

    public int a;
    public Text txt;

    void Start() 
    {
        Debug.Log(TestMultiply(a));
        txt.text = TestMultiply(a).ToString();
	}

}
